/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.venita.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class OXPrograme {

    public OXPrograme() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        String[][] table = {
            {"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinNoPlayBY_X_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow0_BY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1_BY_O_output_true() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2_BY_O_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow0_BY_X_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1_BY_X_output_true() {
        String[][] table = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2_BY_X_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol0_BY_O_output_true() {
        String[][] table = {{"O", "-", "O"}, {"O", "X", "-"}, {"O", "-", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
     @Test
    public void testCheckWinCol1_BY_O_output_true() {
        String[][] table = {{"X", "O", "-"}, {"O", "O", "-"}, {"X", "O", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2_BY_O_output_true() {
        String[][] table = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
     @Test
    public void testCheckWinCol0_BY_X_output_true() {
        String[][] table = {{"X", "-", "O"}, {"X", "O", "-"}, {"X", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
     @Test
    public void testCheckWinCol1_BY_X_output_true() {
        String[][] table = {{"X", "X", "O"}, {"O", "X", "O"}, {"-", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2_BY_X_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDiagonal1_X() {
        String[][] table = {{"X", "O", "-"}, {"O", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckDiagonal2_X() {
        String[][] table = {{"-", "O", "X"}, {"O", "X", "-"}, {"X", "-", "O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckDiagonal1_O() {
        String[][] table = {{"O", "-", "-"}, {"-", "O", "-"}, {"X", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDiagonal2_O() {
        String[][] table = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDraw() {
        String[][] table = {{"X", "O", "O"}, {"X", "O", "X"}, {"O", "X", "O"}};
        assertEquals(true, OXProgram.checkDraw(table));
    }
}

