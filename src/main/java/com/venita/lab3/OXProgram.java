/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.venita.lab3;

/**
 *
 * @author User
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }  
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        if (checkDiagonal(table, currentPlayer)) {
            return true;
        }
        
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }
    
    private static boolean checkDiagonal(String[][] table, String currentPlayer){
        if(checkDiagonal1(table,currentPlayer) || checkDiagonal2(table,currentPlayer)) {
            return true;
        }
        return false;
    }

  private static boolean checkDiagonal1(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }
  
 private static boolean checkDiagonal2(String[][] table, String currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }
 
    static boolean checkDraw(String[][] table) {
        int num = 0;
        for (int row = 0; row < 3; row++) {                 
            for(int col = 0; col < 3; col++) {
                if (!"-".equals(table[row][col])) {                   // เงื่อนไขในการหาว่าช่องกระดานนั้นมีตัวหมากอยู่หรือไม่
                    num++;                                              // ตัวแปรเก็บค่าเพื่อนับจำนวนตัวหมากที่เคยลงเล่นในเกมนี้
                }
            }
        }
        if(num >= 9) {                                          
            return true;
        }else {
            return false;
        }
    }
}

